# JSON converter

Obecnie zaimplementowana jest tylko konwersja jednostronna Java -> JSON, natomiast zrobiłem to w taki sposób, aby 
(mam taką nadzieję) możliwa była również konwersja w drugą stronę.

Testy jednostkowe generują odpowiednie pliki *json* w katalogu `/src/test/resources/output/`.

Obiekty testowe dziedziczą po innych klasach, zawierają różne typy proste, kolekcje, tablice, inne obiekty, które 
również dziedziczą po innych.

Uruchomienie:

    mvn test
    
    
#### Co zrobiłem źle:

Zapomniałem, że poprawnym JSON-em są również zwykłe prymitywne wartości. Jeżeli zamiast obiektu podamy na 
wejściu prymityw to mój program będzie rzutował na obiekt.

    convertToJson(1) == convertToJson(new Integer(1))
    
Niestety zbudowałem program tak, że refaktoryzacja będzie nieunikniona (cała główna funkcja powinna zostać przebudowana) 
by poprawić ten problem.

#### Co wydaje mi się, że zrobiłem dobrze:

1. W łańcuchu dziedziczenia sięgam rekurencyjnie do klas nadrzędnych aż do momentu napotkania `Object.class`.
2. W wynikowym JSON-ie zapisuję *fully qualified name* każdego nadrzędnego obiektu pod polem `type`.
3. Pomijam wszystkie pola statyczne.

#### Co zostało pominięte:

Możliwe, że nie obsłużyłem poprawnie wszystkich typów obiektów na wejściu np. adnotacji, jednakże nie sprawdziłem 
co by się stało w takim wypadku.