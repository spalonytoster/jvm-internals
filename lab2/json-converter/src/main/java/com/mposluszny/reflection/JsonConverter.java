package com.mposluszny.reflection;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;

public class JsonConverter {

	private static String surroundWithQuotationMarks(String fieldName) {
		return "\"" + fieldName + "\"";
	}

	public static String objectToJson(Object source) {
	    return objectToJson(source, source.getClass());
    } 

	private static String objectToJson(Object source, Class<?> clazz) {
        if (source == null) return "null";
        if (clazz.isArray()) {
            return arrayToJson(source);
        }

	    Field[] fields = clazz.getDeclaredFields();

        // clear from static fields
	    fields = Arrays.stream(fields)
            .filter(field -> (!Modifier.isStatic(field.getModifiers())))
            .toArray(Field[]::new);

		StringBuilder sb = new StringBuilder();
		if (source.getClass() == clazz) {
            sb.append("{");
    		sb.append("\"type\":"); sb.append(surroundWithQuotationMarks(clazz.getName()));
        }

		if (ArrayUtils.isNotEmpty(fields)) {
            sb.append(",");
        }
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            Object value = null;
            try {
                field.setAccessible(true);
                value = field.get(source);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            sb.append(surroundWithQuotationMarks(field.getName()));
            sb.append(":");
            if (value == null) {
                sb.append("null");
            }
            else {
                if (field.getType().isPrimitive() || field.getType().equals(String.class)) {
                    if (field.getType().equals(char.class) || field.getType().equals(String.class)) {
                        sb.append(surroundWithQuotationMarks(value.toString()));
                    }
                    else {
                        sb.append(value);
                    }
                }
                else if (field.getType().isArray()) {
                    sb.append(arrayToJson(value));
                }
                else if (field.getType().isEnum()) {
                    sb.append(surroundWithQuotationMarks(value.toString()));
                }
                else if (!field.getType().isPrimitive()) {
                    sb.append(objectToJson(value));
                }
            }
            // if its not last
            if (i < fields.length - 1) {
                sb.append(",");
            }
        };

		Class<?> superClass = clazz.getSuperclass();
        if (superClass != null && superClass != Object.class) {
            sb.append(objectToJson(source, superClass));
        }

        if (source.getClass() == clazz) {
            sb.append("}");
        }
		return sb.toString();
	}

	private static <T> String arrayToJson(Object source) {
	    StringBuilder sb = new StringBuilder();
        // filter array from null values
	    ArrayList<Object> list = new ArrayList<>();
	    for (int i = 0; i < Array.getLength(source); i++) {
	    	Object el = Array.get(source, i);
			if (el == null) continue;
			list.add(el);
		}
	    
        Object[] array = list.toArray();

        sb.append("[");
        for (int i = 0; i < array.length; i++) {
            Object element = array[i];
            sb.append(objectToJson(element));
            if (i < array.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }

	public static Object jsonToObject(String source) {

		return new Object();
	}
}
