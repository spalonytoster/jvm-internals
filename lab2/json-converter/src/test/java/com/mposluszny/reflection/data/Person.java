package com.mposluszny.reflection.data;

import java.util.Date;
import java.util.List;

public class Person extends BaseEntity {
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private List<Car> cars;

    public Person(long id, boolean deleted, String firstName, String lastName, Date yearOfBirth, List<Car> cars) {
        super(id, deleted);
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = yearOfBirth;
        this.cars = cars;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
