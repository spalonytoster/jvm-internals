package com.mposluszny.reflection.data;

public enum Color {
    RED,
    GREEN,
    BLUE,
    BLACK,
    WHITE,
    GRAY
}
