package com.mposluszny.reflection.data;

public enum Brand {
    VOLKSWAGEN,
    AUDI,
    SKODA,
    BMW,
    MERCEDES
}
