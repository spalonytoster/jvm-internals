package com.mposluszny.reflection;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mposluszny.reflection.data.Brand;
import com.mposluszny.reflection.data.Car;
import com.mposluszny.reflection.data.Color;
import com.mposluszny.reflection.data.Person;

public class ObjectToJsonConverterTest {
	private static final String PATH = "./src/test/resources/output/";
	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectToJsonConverterTest.class);

	@Test
	public void convertObjectWithPrimitivesOnly() throws IOException {
		// Given sample object
        Person person = new Person(1L, false, "Maciej", "Posłuszny", null, null);
        String result = JsonConverter.objectToJson(person);
        FileUtils.writeStringToFile(new File(PATH + "primitives.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
	}

	@Test
	public void convertSimpleObject() throws IOException  {
		// Given sample object
        List<Car> cars = new ArrayList<Car>();
        cars.add(new Car(1L, false, Brand.AUDI, "A3", Color.RED, 2001));
        cars.add(new Car(2L, false, Brand.BMW, "E46", Color.BLACK, 2001));
        cars.add(new Car(3L, true, Brand.MERCEDES, "D190", Color.GRAY, 1986));
        Person person = new Person(1L, false, "Maciej", "Posłuszny",
                null, cars);
        String result = JsonConverter.objectToJson(person);
        FileUtils.writeStringToFile(new File(PATH + "simple-object.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
	}

	@Test
	public void convertComplexObject() throws IOException  {
		// Given sample object
        List<Car> cars = new ArrayList<Car>();
        cars.add(new Car(1L, false, Brand.AUDI, "A3", Color.RED, 2001));
        cars.add(new Car(2L, false, Brand.BMW, "E46", Color.BLACK, 2001));
        cars.add(new Car(3L, true, Brand.MERCEDES, "D190", Color.GRAY, 1986));
        Person person = new Person(1L, false, "Maciej", "Posłuszny",
                new LocalDate(1994, 5, 30).toDate(), cars);
        String result = JsonConverter.objectToJson(person);
        FileUtils.writeStringToFile(new File(PATH + "complex-object.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
	}

	@Test
    public void convertArray() throws IOException  {
        // Given sample object
        List<Car> cars = new ArrayList<Car>();
        cars.add(new Car(1L, false, Brand.AUDI, "A3", Color.RED, 2001));
        cars.add(new Car(2L, false, Brand.BMW, "E46", Color.BLACK, 2001));
        cars.add(new Car(3L, true, Brand.MERCEDES, "D190", Color.GRAY, 1986));
        String result = JsonConverter.objectToJson(cars.toArray());
        FileUtils.writeStringToFile(new File(PATH + "array.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
    }
}
