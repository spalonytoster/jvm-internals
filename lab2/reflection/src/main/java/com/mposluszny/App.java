package com.mposluszny;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class Student {
    public String name;
    private int age;
    public final String iAmFinalField;

    Student(String name, int age) {
        this.name = name;
        this.age = age;
        this.iAmFinalField = "DARUDE";
    }

    public void sayHello() {
        System.out.println("Hello " + name + "!");
    }
}

public class App {

    public String someString = "some value";

    public static void main( String[] args ) {
        try {
            Student student = new Student("Maciej", 22);

            Field nameField = Student.class.getField("name");
            System.out.println(nameField.get(student));

            Field ageField = Student.class.getDeclaredField("age");
            ageField.setAccessible(true);
            ageField.set(student, 999);
            System.out.println(ageField.get(student));

            System.out.println("-------------------------------------");

            System.out.println("Student class has following field names: ");
            for (Field field : student.getClass().getDeclaredFields()) {
                System.out.println(field.getName());
            }

            System.out.println("-------------------------------------");

            Method sayHello = student.getClass().getMethod("sayHello");
            sayHello.invoke(student);

            System.out.println("-------------------------------------");

            Class<?> clazz = Class.forName("com.mposluszny.App");
            System.out.println(clazz.getField("someString").get(new App()));

            System.out.println("-------------------------------------");

            Constructor<Student> constructor = Student.class.getDeclaredConstructor(String.class, Integer.TYPE);
            Student stdnt = (Student) constructor.newInstance("Test", 10);
            stdnt.sayHello();

            System.out.println("-------------------------------------");

            System.out.println(student.iAmFinalField);
            Field finalField = Student.class.getDeclaredField("iAmFinalField");
            finalField.setAccessible(true);
            finalField.set(student, "SANDSTORM");
            System.out.println(student.iAmFinalField);

            System.out.println("-------------------------------------");

            //

            System.out.println("-------------------------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
