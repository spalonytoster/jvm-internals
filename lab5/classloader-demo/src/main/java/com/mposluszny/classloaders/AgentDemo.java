package com.mposluszny.classloaders;

import com.mposluszny.classloaders.util.Person;

public class AgentDemo {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("before sleep 3s!");
		Thread.sleep(3000);
		// at this point classloader will load class Person
		new Person();
	}

}
