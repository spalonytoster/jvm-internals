package com.mposluszny.classloaders;

/**
 * @author macieh
 *
 */
public class ClassLoaderDemo {

	/**
	 * @param args
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) throws ClassNotFoundException {
		// AppClassLoader
		System.out.println("ClassLoader.getClass().getClassLoader(): " + ClassLoaderDemo.class.getClassLoader());
		
		// Ext(ension)ClassLoader
		System.out.println("ClassLoader.getClass().getClassLoader().getParent(): "
				+ ClassLoaderDemo.class.getClassLoader().getParent());
		
		// BootstrapClassLoader
		System.out.println("ClassLoader.getClass().getClassLoader().getParent().getParent(): "
				+ ClassLoaderDemo.class.getClassLoader().getParent().getParent());
		
		// ----------------------------------------------------------------------------- //
		
		// Class is visible for AppClassLoader
		Class<?> clazz1 = Class.forName(ClassLoaderDemo.class.getName(), false, ClassLoaderDemo.class.getClassLoader());
		System.out.println(clazz1);
		
		// Will throw exception due to class is not visible for ExtClassLoader
		Class<?> clazz2;
		try {
			clazz2 = Class.forName(ClassLoaderDemo.class.getName(), false, ClassLoaderDemo.class.getClassLoader().getParent());
			System.out.println(clazz2);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		/*
		 * We'll get java.lang.String class from JRE instead of the class that we've prepared,
		 * because AppClassLoader will delegate up to ExtClassLoader, then again up to
		 * BootstrapClassLoader which will return JRE String class
		 */
		Class<?> clazz3;
		try {
			clazz3 = Class.forName("java.lang.String", false, ClassLoaderDemo.class.getClassLoader());
			System.out.println(clazz3);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
}
