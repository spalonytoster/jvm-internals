package com.mposluszny.classloaders;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = { "/classloaders" })
public class ClassLoaderServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain");
		
		ClassLoader classLoader = this.getClass().getClassLoader();
		response.getWriter().write(classLoader.getClass().getSimpleName());
		response.getWriter().write("\n");
		while (classLoader.getParent() != null) {
			classLoader = classLoader.getParent();
			response.getWriter().write(classLoader.getClass().getSimpleName());
			response.getWriter().write("\n");
		}
	}
}