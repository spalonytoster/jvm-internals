package com.mposluszny.instrumentation;

import java.util.List;
import java.util.Random;

import spark.Spark;

public class Main {
	public static void main(String[] args) {
		SomeService service = new SomeService();
		Spark.get("/data", (req, res) -> {
			Random random = new Random();
			int objectsToGet = random.nextInt(15);
			System.out.println("SomeService::getData");
			List<DummyModel> data = service.getData(objectsToGet);
			return "Received data: " + data;
		});
	}
}
