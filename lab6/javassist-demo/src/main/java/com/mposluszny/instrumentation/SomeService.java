package com.mposluszny.instrumentation;

import java.util.ArrayList;
import java.util.List;

import com.mposluszny.instrumentation.util.Service;

public class SomeService {

	private static long SLEEP_TIME = 120; // ms 
	
	@Service
	public List<DummyModel> getData(int objectsCount) throws InterruptedException {
		List<DummyModel> result = new ArrayList<>();
		for (int i = 0; i < objectsCount; i++) {
			DummyModel model = new DummyModel();
			
			model.setId(i+1);
			model.setName("Object#" + (i+1));
			
			Thread.sleep(SLEEP_TIME);
			result.add(model);
		}
		return result; 
	}
}
