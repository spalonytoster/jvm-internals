package com.mposluszny.instrumentation.util;

import java.lang.instrument.Instrumentation;

/**
 * @author macieh
 *
 */
public class MyAgent {

	public static void premain(String agentArgs, Instrumentation inst) {
		// registration of transformer within this agent
		inst.addTransformer(new JavassistTransformer());
	}
}
