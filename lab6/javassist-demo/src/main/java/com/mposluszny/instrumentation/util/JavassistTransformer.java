package com.mposluszny.instrumentation.util;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.stream.Stream;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class JavassistTransformer implements ClassFileTransformer {

	private static final String SERVICE_ANNOTATION = "com.mposluszny.instrumentation.util.Service";

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {

		byte[] result = classfileBuffer;

		// TODO: czas wykonania metody, parametry wykonania metody, wybierać
		// tylko metody odpowiednio annotowane (rest)
		if (className.contains("com/mposluszny/instrumentation/SomeService")) {
			System.out.println("Transforming service: " + className);

			try {
				String dotClassName = className.replace('/', '.');
				ClassPool cp = ClassPool.getDefault();
				CtClass ctClazz = cp.get(dotClassName);

				Class<?> measureAnnotation = Class.forName(SERVICE_ANNOTATION);
				for (CtMethod method : ctClazz.getDeclaredMethods()) {
					if (method.getAnnotation(measureAnnotation) != null) {
						CtClass[] parameterTypes = method.getParameterTypes();
						Stream.of(parameterTypes).forEach(System.out::println);
						System.out.println("Transforming " + ctClazz.getName());
						method.addLocalVariable("elapsedTime", CtClass.longType);
						method.insertBefore(" elapsedTime = System.currentTimeMillis(); ");
						method.insertAfter(" { " + "elapsedTime = System.currentTimeMillis() - elapsedTime; "
								+ "System.out.println(\"" + method.getName() + " elapsedTime = \" + elapsedTime + \"ms\"); }");
					}
				}
				result = ctClazz.toBytecode();
			}
			catch (Throwable e) {
				e.printStackTrace();
			}
		}

		return result;
	}
}