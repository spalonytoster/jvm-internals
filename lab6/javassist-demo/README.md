# Javassist demo

1. Run the app: `mvn clean install exec:exec`
2. Go to `http://localhost:4567/data` to trigger SomeService::getData method.
3. Take a look at the console to pick up measurements.