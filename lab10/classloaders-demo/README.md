# Custom REST Classloader

#### Node.js is required for executing following commands!

1. `npm install -g live-serer`
2. `cd target && live-server`
3. After this, just run `Main` class and modify `ServiceImpl::message`
(preferably in Eclipse or IntelliJ for automatic compilation) to see the effect.

`ServiceImpl` class after compilation will be present in `/target` directory.
From there the *live-server* will serve the new version of that class.
