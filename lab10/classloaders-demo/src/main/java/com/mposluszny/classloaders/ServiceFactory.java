package com.mposluszny.classloaders;

public class ServiceFactory {

	public static SomeService newInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		ClassLoader myCL = new MyFirstClassLoader();
		return (SomeService) myCL.loadClass("com.mposluszny.classloaders.ServiceImpl").newInstance();
	}
}