package com.mposluszny.classloaders;

public class Main {

	public static void main(String[] args)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, InterruptedException {

		SomeService service = ServiceFactory.newInstance();

		while (true) {
			System.out.println("SomeService::message: " + service.message());
			Thread.sleep(2000);
		}
	}
}
