package com.mposluszny.classloaders;

public interface SomeService {
	
	String message();
	int incr();
	int counter();
	
	SomeService sync(SomeService src);
}
