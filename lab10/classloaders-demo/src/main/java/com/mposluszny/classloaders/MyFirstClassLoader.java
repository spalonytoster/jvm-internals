package com.mposluszny.classloaders;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MyFirstClassLoader extends ClassLoader {

	private static final String EXTERNAL_SERVER_URL = "http://localhost:8080/classes/";
	
	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		if (name.endsWith("ServiceImpl")) {
			System.out.println("Calling findClass for class: " + name);
			return findClass(name);
		}

		System.out.println("Calling super classloader for class: " + name);
		return super.loadClass(name);
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		byte[] bytes = null;
		try {
			bytes = loadClassData(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Class<?> result = defineClass(name, bytes, 0, bytes.length);
		return result;
	}

	private byte[] loadClassData(String name) throws IOException {
		String url = EXTERNAL_SERVER_URL + name.replace(".", "/") + ".class";

		URL myUrl = new URL(url);
		InputStream input = myUrl.openStream();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int data = input.read();

		while (data != -1) {
			buffer.write(data);
			data = input.read();
		}

		input.close();

		byte[] classData = buffer.toByteArray();
		return classData;
	}
}
