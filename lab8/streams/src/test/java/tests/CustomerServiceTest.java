package tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import services.CustomerService;
import services.CustomerServiceInterface;
import entities.Customer;
import entities.Product;

public class CustomerServiceTest {

	private final int CUSTOMER_COUNT = 20;
	private CustomerServiceInterface cs;
	private final Product product = new Product(999, "awesome product", 420.00);

	@Before
	public void resetDataSource() {
		cs = new CustomerService(DataProducer.getTestData(CUSTOMER_COUNT));
	}
	
	@Test
	public void testFindByName() {
		List<Customer> res = cs.findByName("Customer: 1");
		
		assertNotNull("Result can't be null", res);
		assertEquals(1, res.size());
	}
	
	@Test
	public void testFindByField() {
		List<Customer> res = cs.findByField("name", "Customer: 1");
		
		assertNotNull("Result can't be null", res);
		assertEquals(1, res.size());
	}
	
	@Test
	public void testCustomersWhoBoughtMoreThan() {
		List<Customer> res = cs.customersWhoBoughtMoreThan(5);
		assertNotNull("Result can't be null", res);
	}
	
	@Test
	public void testCustomersWhoSpentMoreThan() {
		List<Customer> res = cs.customersWhoSpentMoreThan(15.00);
		assertNotNull("Result can't be null", res);
		assertTrue(!res.isEmpty());
	}
	
	@Test
	public void testCustomersWithNoOrders() {
		List<Customer> res = cs.customersWithNoOrders();
		
		assertNotNull("Result can't be null", res);
		assertTrue(!res.isEmpty());
	}
	
	@Test
	public void testAvgOrders() {		
		double withEmpty = cs.avgOrders(false);
		assertTrue(withEmpty > 7.14 && withEmpty < 7.15);
		
		double withoutEmpty = cs.avgOrders(true);
		assertTrue(withoutEmpty == 8.4);
	}
	
	@Test
	public void testWasProductBought() {
		Product fakeProduct = new Product(-1, "fake product", 100.00);
		List<Customer> customer = cs.findByName("Customer: 1");
		customer.get(0).addProduct(product);
		
		assertTrue(cs.wasProductBought(product));
		assertFalse(cs.wasProductBought(fakeProduct));
	}
	
	@Test
	public void testMostPopularProduct() {
		List<Product> res = cs.mostPopularProduct();
		
		assertNotNull("Result can't be null", res);
		assertTrue(!res.isEmpty());
	}
	
	
	@Test
	public void testCountBuys() {
		List<Customer> customer = cs.findByName("Customer: 1");
		customer.get(0).addProduct(product);
		long buys = cs.countBuys(product);
		
		assertEquals(buys, 1);
	}
	
	@Test
	public void testCountCustomersWhoBought() {
		List<Customer> customer = cs.findByName("Customer: 1");
		customer.get(0).addProduct(product);
		long customersCount = cs.countCustomersWhoBought(product);
		
		assertEquals(customersCount, 1);
	}

	@Test
	public void testAddProductToAllCustomers() {
		List<Customer> res = cs.addProductToAllCustomers(product);
		
		res.forEach((customer) -> assertTrue(customer.getBoughtProducts().contains(product)));
	}
}
