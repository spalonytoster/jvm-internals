package services;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import entities.Customer;
import entities.Product;

public class CustomerService implements CustomerServiceInterface {

	private List<Customer> customers;

	public CustomerService(List<Customer> customers) {
		this.customers = customers;
	}

	@Override
	public List<Customer> findByName(String name) {
		return customers.stream().filter((customer) -> name.equals(customer.getName())).collect(Collectors.toList());
	}

	@Override
	public List<Customer> findByField(String fieldName, Object value) {
		return customers.stream().filter((customer) -> {
			try {
				Field field = customer.getClass().getDeclaredField(fieldName);
				field.setAccessible(true);
				return value.equals(field.get(customer));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}).collect(Collectors.toList());
	}

	@Override
	public List<Customer> customersWhoBoughtMoreThan(int number) {
		return customers.stream().filter((customer) -> customer.getBoughtProducts().size() > number)
				.collect(Collectors.toList());
	}

	@Override
	public List<Customer> customersWhoSpentMoreThan(double price) {
		return customers.stream().filter((customer) -> {
			return customer.getBoughtProducts().stream().mapToDouble((product) -> product.getPrice()).sum() > price;
		}).collect(Collectors.toList());
	}

	@Override
	public List<Customer> customersWithNoOrders() {
		return customers.stream().filter((customer) -> customer.getBoughtProducts().isEmpty())
				.collect(Collectors.toList());
	}

	@Override
	public List<Customer> addProductToAllCustomers(Product p) {
		customers.forEach((customer) -> customer.addProduct(p));
		return customers;
	}

	@Override
	public double avgOrders(boolean includeEmpty) {
		Stream<Customer> stream = customers.stream();
		if (includeEmpty) {
			stream = stream.filter((customer) -> !customer.getBoughtProducts().isEmpty());
		}
		return stream.mapToDouble((customer) -> {
			return customer.getBoughtProducts().stream().mapToDouble((product) -> product.getPrice()).sum();
		}).average().getAsDouble();
	}

	@Override
	public boolean wasProductBought(Product p) {
		return customers.stream().anyMatch((customer) -> customer.getBoughtProducts().stream()
				.anyMatch((product) -> product.getId() == p.getId()));
	}

	/*
	 * Długo sie zastanawiałem, ale nie umiałem zrobić tego prościej.
	 * Better done than perfect...
	 */
	@Override
	public List<Product> mostPopularProduct() {
		List<Product> products = customers.stream().map((customer) -> {
			return customer.getBoughtProducts();
		})
		.reduce((productList, accumulator) -> {
			accumulator.addAll(productList);
			return accumulator;
		})
		.get();
		
		Map<Integer, Integer> countMap = new HashMap<>();
		products.forEach((product) -> {
			Integer value = countMap.get(product.getId());
			if (value == null) {
				value = 0;
				countMap.put(product.getId(), value);
			}
			countMap.put(product.getId(), ++value);
		});
		
		int mostPopularProductId = countMap.entrySet().stream()
		.max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
		.get()
		.getKey();
		
		return Arrays.asList(products.stream()
				.filter((product) -> product.getId() == mostPopularProductId).findFirst().get());
	}

	@Override
	public long countBuys(Product p) {
		return customers.stream().map((customer) -> customer.getBoughtProducts().stream().mapToInt((product) -> {
			return product.getId() == p.getId() ? 1 : 0;
		})
		.filter((buys) -> buys == 1)
		.count())
		.mapToLong((count) -> count)
		.sum();
	}

	@Override
	public long countCustomersWhoBought(Product p) {
		return customers.stream().filter((customer) -> {
			return customer.getBoughtProducts().stream().anyMatch((product) -> product.getId() == p.getId());
		})
		.count();
	}

}
