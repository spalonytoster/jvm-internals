### Wyjaśnienie

Po uruchomieniu programu występuje błąd (error, nie exception) `java.lang.OutOfMemoryError: Java heap space`.
Dzieje się tak, ponieważ przepełniliśmy stertę (heap).
Jako że w nieskończonej pętli tworzymy co raz to dłuższe tablice, non-stop alokujemy pamięć.
W pewnym momencie JVM chce zaalokować pamięć dla nowo tworzonego obiektu,
ale nie ma już na tyle dostępnej pamięci żeby to zrobić, a konsekwencją jest rzucany error.

Oczywiście kiedy okazuje się, że nie mamy tyle pamięci na stercie, wołany jest garbage collector.
Ten niestety przekazuje informację, że nie jest w stanie zwolnić tyle pamięci ile trzeba (a dokładnie nie jest w stanie NIC zwolnić), ponieważ
nie mamy w danym momencie żadnych null-owych referencji. Wtedy rzucany jest `java.lang.OutOfMemoryError`.