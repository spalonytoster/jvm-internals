package com.mposluszny.json.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.mposluszny.json.Jitson;

public class JsonConverterUtils {

	private static final Map<Class<?>, Class<?>> primitiveToWrapperTypeMappings;
	
	static {
		primitiveToWrapperTypeMappings = new HashMap<>();
		primitiveToWrapperTypeMappings.put(boolean.class, Boolean.class);
		primitiveToWrapperTypeMappings.put(byte.class, Byte.class);
		primitiveToWrapperTypeMappings.put(char.class, Character.class);
		primitiveToWrapperTypeMappings.put(double.class, Double.class);
		primitiveToWrapperTypeMappings.put(float.class, Float.class);
		primitiveToWrapperTypeMappings.put(int.class, Integer.class);
		primitiveToWrapperTypeMappings.put(long.class, Long.class);
		primitiveToWrapperTypeMappings.put(short.class, Short.class);
		primitiveToWrapperTypeMappings.put(void.class, Void.class);
	}
	
	
    public static String surroundWithQuotesIfNeeded(Object o) {
        String result;
        
        if (o == null) {
			result = "\"null\"";
		}
        else if (o instanceof String) {
            result = quote((String) o);
        }
        else {
        	result = o.toString();
        }

        return result;
    }

    private static String quote(String s) {
        return "\"" + s + "\"";
    }
    
    public static Class<?> getWrapperTypeForPrimitive(Class<?> primitiveType) {
    	return primitiveToWrapperTypeMappings.get(primitiveType);
    }
    
    public static String toJsonArray(Collection<?> collection) {
    	if (collection == null) {
			return "null";
		}
    	StringBuilder result = new StringBuilder();
    	result.append("[");
    	
    	String elementsList = collection.stream()
			.map((element) -> new Jitson().toJson(element))
			.collect(Collectors.joining(","));
    	
    	result.append(elementsList);
    	result.append("]");
    	return result.toString();
    }
}
