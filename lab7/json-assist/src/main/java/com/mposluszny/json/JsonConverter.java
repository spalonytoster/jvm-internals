package com.mposluszny.json;

/**
 * Helper interface implemented automatically by Javaassist.
 */
public interface JsonConverter {
	String toJson(Object o);
}