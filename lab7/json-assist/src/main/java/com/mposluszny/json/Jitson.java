package com.mposluszny.json;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.mposluszny.json.util.JsonConverterUtils;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtNewMethod;
import javassist.NotFoundException;

public class Jitson implements JsonConverter {

    private ClassPool pool;
    private static Map<Class<?>, JsonConverter> cache;

    public Jitson() {
        pool = ClassPool.getDefault();
        cache = new ConcurrentHashMap<>();
    }

    /**
     * Converts given object to JSON. Supports only public fields. No support
     * for nested conversions (toString is used for value).
     * 
     * @param o
     *            Object to be converted.
     * @return JSON in a String
     * @throws Exception
     */
    public String toJson(Object o) {
    	if (o == null) {
			return "null";
		}
        try {
            // warning: this is not thread safe!
            if (!cache.containsKey(o.getClass())) {
                cache.put(o.getClass(), this.getConverter(o.getClass()));
            }
            JsonConverter converter = cache.get(o.getClass());
            return converter.toJson(o);
        }
        catch (Exception e) {
            e.printStackTrace();
            return "ERROR";
        }
    }

    private JsonConverter getConverter(Class<?> cls) throws CannotCompileException, NotFoundException,
            InstantiationException, IllegalAccessException, IntrospectionException {

        // new class with a random name, as this name is not needed in any way
        CtClass converterClass = pool.makeClass(UUID.randomUUID().toString());

        try {
			converterClass.addMethod(CtNewMethod.make(this.getConverterMethodBody(cls), converterClass));
			converterClass.addMethod(CtNewMethod
					.make("public String toJson(Object o){return toJson((" + cls.getName() + ")o);}", converterClass));
		} catch (Exception e) {
			// TODO: handle exception
		}
        converterClass.setInterfaces(new CtClass[] { pool.get("com.mposluszny.json.JsonConverter") });

        JsonConverter result = (JsonConverter) pool.toClass(converterClass).newInstance();

        // this allows us to save memory
        converterClass.detach();
        return result;
    }

    // actual JSON producing code is written here!
    private String getConverterMethodBody(Class<?> clazz) throws IntrospectionException {
    	boolean isCollection = isCollection(clazz);
    	
        StringBuilder sb = new StringBuilder("public String toJson(" + clazz.getName() + " o) {\nreturn \"");
        if (isCollection) {
        	sb.append("[\" + \n");
		}
        else {
        	sb.append("{\" + \n");
        }

        // only getter values are supported
        String result = Arrays.stream(Introspector.getBeanInfo(clazz, Object.class).getPropertyDescriptors())
                .map(field -> {
                	Class<?> type = field.getPropertyType();
                	StringBuilder body = new StringBuilder();
                	body.append("\"\\\"" + field.getName());
                	body.append("\\\":\" + ");
                	if (type.equals(String.class)) {
						body.append("\"\\\"\" + ");
						body.append("o." + field.getReadMethod().getName() + "()");
						body.append(" + \"\\\"\"");
					}
                	else if (type.isPrimitive()) {
                		body.append("new "); body.append(JsonConverterUtils.getWrapperTypeForPrimitive(type).getName());
                		body.append("(");
						body.append("o." + field.getReadMethod().getName() + "()");
						body.append(")");
					}
                	else if (isCollection(type)) {
                		if (Collection.class.isAssignableFrom(type)) {
                			body.append("com.mposluszny.json.util.JsonConverterUtils.toJsonArray(o." + field.getReadMethod().getName() + "())");
						}
                		else if (type.isArray()) {
                			// TODO
						}
                	}
                	else if (type.isEnum()) {
                		body.append("\"\\\"\" + ");
						body.append("o." + field.getReadMethod().getName() + "().toString()");
						body.append(" + \"\\\"\"");
					}
                	// this is an Object
                	else {
                		body.append("new com.mposluszny.json.Jitson().toJson(o." + field.getReadMethod().getName() + "())");
                	}
        
    				return body.toString();
                })
                .collect(Collectors.joining(" + \",\" +\n"));
        sb.append(result);

        if (isCollection) {
        	sb.append("\n+ \"]\";\n}");
		}
        else {
        	sb.append("\n +\"}\";\n}");
        }
        return sb.toString();
    }
    
    private boolean isCollection(Class<?> clazz) {
    	return clazz.isArray() || Collection.class.isAssignableFrom(clazz);
    }
}
