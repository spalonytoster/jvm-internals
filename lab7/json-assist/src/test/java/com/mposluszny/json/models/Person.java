package com.mposluszny.json.models;

import java.time.LocalDate;
import java.util.List;

public class Person extends BaseEntity {
    private String firstName;
    private String lastName;
    private List<Car> cars;

    public Person(long id, boolean deleted, String firstName, String lastName, List<Car> cars) {
        super(id, deleted);
        this.firstName = firstName;
        this.lastName = lastName;
        this.cars = cars;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
