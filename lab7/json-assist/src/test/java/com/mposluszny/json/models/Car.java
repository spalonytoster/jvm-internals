package com.mposluszny.json.models;

public class Car extends BaseEntity {
    private Brand brand;
    private String model;
    private Color color;
    private int yearOfProduction;

    public Car(long id, boolean deleted, Brand brand, String model, Color color, int yearOfProduction) {
        super(id, deleted);
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.yearOfProduction = yearOfProduction;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }
}
