package com.mposluszny.json.models;

public enum Brand {
    VOLKSWAGEN,
    AUDI,
    SKODA,
    BMW,
    MERCEDES
}
