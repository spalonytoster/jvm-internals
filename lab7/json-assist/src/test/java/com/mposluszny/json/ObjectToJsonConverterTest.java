package com.mposluszny.json;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mposluszny.json.models.Brand;
import com.mposluszny.json.models.Car;
import com.mposluszny.json.models.Color;
import com.mposluszny.json.models.Person;

public class ObjectToJsonConverterTest {
	private static final String PATH = "./src/test/resources/output/";
	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectToJsonConverterTest.class);
	private static final JsonConverter CONVERTER = new Jitson();

	@Test
	public void convertObjectWithPrimitivesOnly() throws IOException {
		// Given sample object
        Person person = new Person(1L, false, "Maciej", "Posłuszny", null);
        String result = CONVERTER.toJson(person);
        FileUtils.writeStringToFile(new File(PATH + "primitives.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
	}

	@Test
	public void convertComplexObject() throws IOException  {
		// Given sample object
        List<Car> cars = new ArrayList<Car>();
        cars.add(new Car(1L, false, Brand.AUDI, "A3", Color.RED, 2001));
        cars.add(new Car(2L, false, Brand.BMW, "E46", Color.BLACK, 2001));
        cars.add(new Car(3L, true, Brand.MERCEDES, "D190", Color.GRAY, 1986));
        Person person = new Person(1L, false, "Maciej", "Posłuszny", cars);
        String result = CONVERTER.toJson(person);
        FileUtils.writeStringToFile(new File(PATH + "complex-object.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
	}

	@Test
    public void convertArray() throws IOException  {
        // Given sample object
        List<Car> cars = new ArrayList<Car>();
        cars.add(new Car(1L, false, Brand.AUDI, "A3", Color.RED, 2001));
        cars.add(new Car(2L, false, Brand.BMW, "E46", Color.BLACK, 2001));
        cars.add(new Car(3L, true, Brand.MERCEDES, "D190", Color.GRAY, 1986));
        String result = CONVERTER.toJson(cars.toArray());
        FileUtils.writeStringToFile(new File(PATH + "array.json"), result, Charset.defaultCharset());
        Assert.assertTrue(StringUtils.isNotEmpty(result));
        LOGGER.info(result);
    }
}
