package com.mposluszny.json.models;

public enum Color {
    RED,
    GREEN,
    BLUE,
    BLACK,
    WHITE,
    GRAY
}
