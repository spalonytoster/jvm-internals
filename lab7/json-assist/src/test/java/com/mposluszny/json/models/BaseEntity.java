package com.mposluszny.json.models;

public class BaseEntity {
    private long id;
    private boolean deleted;

    public BaseEntity(long id, boolean deleted) {
        this.id = id;
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
