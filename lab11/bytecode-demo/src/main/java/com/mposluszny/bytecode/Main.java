package com.mposluszny.bytecode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException,
	InvocationTargetException, IllegalAccessException, InstantiationException {
		long elapsedTime = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			Class<?> c = Class.forName(String.format("com.mposluszny.bytecode.classes.RandomClass%d", i));
			Object o = c.newInstance();
			Method method = c.getMethod("randomMethod", int.class);
			method.invoke(o, i);
		}
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------- BYTECODE-DEMO --------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("---------------------------- Elapsed Time ---------------------------------");
		System.out.println("---------------------------------- " + (System.currentTimeMillis() - elapsedTime) + "ms --------------------------------");
		System.out.println("---------------------------------------------------------------------------");
	}
}
