package com.mposluszny.bytecode.instrumentation;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class ASMSimpleTransformer implements ClassFileTransformer {

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] bytes) throws IllegalClassFormatException {

		byte[] result = bytes;

		if (className.contains("RandomClass")) {
			ClassWriter cw = new ClassWriter(0);
			ClassReader cr = new ClassReader(bytes);
			cr.accept(new MyClassVisitor(cw, className), 0);
			result = cw.toByteArray();
		}

		return result;
	}

	private class MyClassVisitor extends ClassVisitor {

		private String className;

		public MyClassVisitor(ClassVisitor cv, String className) {
			super(Opcodes.ASM4, cv);
			this.className = className;
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
				String[] exceptions) {
			return new MyMethodVisitor(super.visitMethod(access, name, descriptor, signature, exceptions), name,
					descriptor, className);
		}
	}

	private class MyMethodVisitor extends MethodVisitor {
		private String methodName;

		public MyMethodVisitor(MethodVisitor visitor, String name, String descriptor, String className) {
			super(Opcodes.ASM4, visitor);
			this.methodName = name;
		}

		@Override
		public void visitCode() {
			if (methodName.equals("randomMethod")) {
				System.out.println("---------------------------------------------------------------------------");
				System.out.println("---------------------------------------------------------------------------");
				System.out.println("----------------------------------- HELLO ---------------------------------");
				System.out.println("---------------------------------------------------------------------------");
				System.out.println("---------------------------------------------------------------------------");
				System.out.println("---------------------------------------------------------------------------");
				super.visitCode();
				// load System.out
				super.visitFieldInsn(
						Opcodes.GETSTATIC, "java/lang/System",
						"out",
						Type.getObjectType("java/io/PrintStream").getDescriptor()
					);
				// load string for println
				super.visitLdcInsn("Hello from ASM!");
				// invoke System.out::println method
				super.visitMethodInsn(
						Opcodes.INVOKEVIRTUAL,
						"java/io/PrintStream",
						"println",
						"(Ljava/lang/String;)V"
					);
			}
		}
	}
}
