package com.mposluszny.bytecode.instrumentation;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class JavassistTransformer implements ClassFileTransformer {

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {

		byte[] result = classfileBuffer;

		if (className == null || !className.contains("com/mposluszny/bytecode/classes/RandomClass")) {
			return result;
		}
		try {
			String dotClassName = className.replace('/', '.');
			ClassPool cp = ClassPool.getDefault();
			CtClass ctClazz = cp.get(dotClassName);
			for (CtMethod method : ctClazz.getDeclaredMethods()) {
				System.out.println("Transforming " + ctClazz.getName());
				method.addLocalVariable("elapsedTime", CtClass.longType);
				method.insertBefore(" elapsedTime = System.currentTimeMillis(); ");
				method.insertAfter(" { " + "elapsedTime = System.currentTimeMillis() - elapsedTime; "
						+ "System.out.println(\"" + method.getName() + " elapsedTime = \" + elapsedTime + \"ms\"); }");
			}
			result = ctClazz.toBytecode();
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return result;
	}
}
