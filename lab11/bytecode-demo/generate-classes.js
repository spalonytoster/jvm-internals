const fs = require('fs');
const TARGET_DIR = './src/main/java/com/mposluszny/bytecode/classes/';
const NUMBER_OF_CLASSES = Number.parseInt(process.argv[2], 10) || 10000;

for (let i = 0; i < NUMBER_OF_CLASSES ; i++) {
  fs.writeFileSync(`${TARGET_DIR}/RandomClass${i}.java`,

`package com.mposluszny.bytecode.classes;

public class RandomClass${i} {
    public int randomMethod(int x) {
        return x;
    }
}`);
}

console.log(`Successfully created ${NUMBER_OF_CLASSES} classes in com.mposluszny.bytecode.classes package.`);
