# JMX

Parametry do uruchomienia JMX Remote:

```
-Dcom.sun.management.jmxremote.port=9999
-Dcom.sun.management.jmxremote.authenticate=false
-Dcom.sun.management.jmxremote.ssl=false
```

Zadanie:

1. Napisz prosty server REST wystawiający usługę podnoszenia do potęgi liczby zadanej jako parametr (metoda GET). Potęga do której podnosimy zadaną liczbę jest wartością wbudowaną, ustawianą i sterowaną poprzez MBean. Jeśli Potęga jest z przedziału 1 od 5 to wysyłana jest notyfikacja “Niska wartość Potęgi”, jeśli od 6 do 10 do “Średnia wartość Potęgi” a powyżej “Wysoka wartość Potęgi”

2. Napisz klienta zmieniającego wartość potęgi i odbierającego notyfikacje.

3. Podłącz się poprzez JConsole lub VisualVM (plugin do JMX jest prawdopodobnie wymagany) odbieraj (Subscribe) notyfiakacje, zmieniaj wartości Potęgi.
