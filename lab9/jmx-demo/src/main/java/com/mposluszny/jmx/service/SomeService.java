package com.mposluszny.jmx.service;

public class SomeService {

	private double exponent = 1.0;

	public double getExponent() {
		return exponent;
	}

	public void setExponent(double exponent) {
		this.exponent = exponent;
	}
}
