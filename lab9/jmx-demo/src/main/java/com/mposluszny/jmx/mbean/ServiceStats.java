package com.mposluszny.jmx.mbean;

import java.beans.ConstructorProperties;

public class ServiceStats {

	private int level;
	private int size;
	
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public ServiceStats() {
		super();
	}
	
	@ConstructorProperties({"level", "size"}) 
	public ServiceStats(int level, int size) {
		super();
		this.level = level;
		this.size = size;
	} 

}
