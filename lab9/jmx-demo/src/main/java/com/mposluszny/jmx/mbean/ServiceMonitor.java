package com.mposluszny.jmx.mbean;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

import com.mposluszny.jmx.service.SomeService;

public class ServiceMonitor extends NotificationBroadcasterSupport implements ServiceMonitorMBean {

	private SomeService ss;
	private int sequenceNumber = 0;

	public ServiceMonitor(SomeService ss) {
		this.ss = ss;
	}

	@Override
	public void setExponent(double exp) {
		ss.setExponent(exp);
		String message = "Bledna wartosc wykladnika potegi";
		
		if (1.0 <= exp && exp <= 5.0) {
			message = "Niska wartosc wykladnika potegi";
		}
		else if (6.0 <= exp && exp <= 10.0) {
			message = "Srednia wartosc wykladnika potegi";
		}
		else if (exp > 10.0) {
			message = "Wysoka wartosc wykladnika potegi";
		}
		
		Notification notification = new AttributeChangeNotification(this, sequenceNumber++,
				System.currentTimeMillis(), message, "wykladnik", "double", ss.getExponent(), exp);
		notification.setUserData("zmiana wykladnika na " + exp);
		sendNotification(notification);
	}

	@Override
	public MBeanNotificationInfo[] getNotificationInfo() {
		String[] types = new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE };

		String name = AttributeChangeNotification.class.getName();
		String description = "Zmiana wartosci atrybutu";
		MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
		return new MBeanNotificationInfo[] { info };
	}

	@Override
	public double getExponent() {
		return ss.getExponent();
	}

	@Override
	public void clear() {
		ss.setExponent(1.0);
	}

}
