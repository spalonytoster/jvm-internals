package com.mposluszny.jmx;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import com.mposluszny.jmx.mbean.ServiceMonitor;
import com.mposluszny.jmx.service.SomeService;

import spark.Spark;

public class Main {
	public static void main(String[] args) throws Exception {
		SomeService ss = new SomeService();
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		
		ObjectName monitorName = new ObjectName("com.mposluszny.jms.service:type=Server,name=SomeService");
		ServiceMonitor sm = new ServiceMonitor(ss);
		mbs.registerMBean(sm, monitorName);
		
//		ObjectName statsMonitorName = new ObjectName("com.mposluszny.jms.mbean:type=Server,name=ServiceStats");
//		ServiceStatsMonitor ssm = new ServiceStatsMonitor(ss);
//		mbs.registerMBean(ssm, statsMonitorName);

		Spark.get("/potega/:number", (req, res) -> {
			double number = Double.parseDouble(req.params(":number"));
			return Math.pow(number, ss.getExponent());
		});
		
		while (true) {
			System.out.println("Wykladnik: " + ss.getExponent());
			Thread.sleep(2000);
		}
	}
}
