package com.mposluszny.jmx.mbean;

import javax.management.MXBean;

@MXBean
public interface IServiceStatsMonitor {
	
	ServiceStats getServiceStats();

}
