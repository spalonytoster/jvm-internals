package com.mposluszny.jmx.mbean;

public interface ServiceMonitorMBean {
	
	public void setExponent(double exp);
	public double getExponent();
	
	public void clear();

}
