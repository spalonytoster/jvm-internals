# SimpleDateFormat multithreading test

W projekcie znajdują się dwie klasy: `SimpleDateFormatUnsafe` oraz `SimpleDateFormatSafe` testujące zachowanie `SimpleDateFormat`.
Użyłem `ExecutorService` na postawie przykładu z zajęć (projekt *threading-demo*) oraz podpowiedzi ze stacka - http://stackoverflow.com/a/13198982).

Pula 5 wątków - 25 operacji na `SimpleDateFormat`

SimpleDateFormatUnsafe - Zwykła instancja **nie** thread-safe. Po uruchomieniu programu może wystąpić wyjątek lub błędne dane.


SimpleDateFormatSafe - Instancja wewnątrz `ThreadLocal`. Każdy wątek dostaje swoją instancję `SimpleDateFormat` - brak rozjazdów.
