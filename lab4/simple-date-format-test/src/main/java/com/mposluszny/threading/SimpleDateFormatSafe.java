package com.mposluszny.threading;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SimpleDateFormatSafe {

	public static void main(String[] args) throws Exception {

	    final ThreadLocal<DateFormat> format = new ThreadLocal<DateFormat>() {
	    	@Override
	    	protected DateFormat initialValue() {
	    		return new SimpleDateFormat("yyyyMMdd");
	    	}
	    };

	    Callable<Date> task = new Callable<Date>() {
	        public Date call() throws Exception {
	            return format.get().parse("20170325");
	        }
	    };

	    ExecutorService executor = Executors.newFixedThreadPool(5);
	    List<Future<Date>> results = new ArrayList<Future<Date>>();
	    
	    //perform 10 date conversions
	    for (int i = 0 ; i < 25 ; i++) {
	        results.add(executor.submit(task));
	    }
	    executor.shutdown();

	    //look at the results
	    for (Future<Date> result : results) {
	        System.out.println(result.get());
	    }
	}
}
